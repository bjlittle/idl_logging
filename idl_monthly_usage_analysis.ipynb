{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# IDL Monthly Usage Logging\n",
    "\n",
    "Processing and plotting IDL usage data.\n",
    "\n",
    "Accesses monthly summary files of IDL license information, within monthly directory. Uses pandas to process the monthly data files and produce analysis plots."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "\n",
    "* [Setup](#Setup)\n",
    "* [Load and Process Data](#Load-and-Process-Data)\n",
    "    * [Fine Resolution Totals](#Fine-Resolution-Totals)\n",
    "    * [Daily Totals](#Daily-Totals)\n",
    "    * [SPICE Usage](#SPICE-Usage)\n",
    "    * [Usage by User](#Usage-by-User)\n",
    "    * [Usage by idl_analyst User](#Usage-by-idl_analyst-User)\n",
    "* [Analysis and Plotting](#Analysis-and-Plotting)\n",
    "    * [Generic Plotting Function](#Generic-Plotting-Function)\n",
    "    * [Plot Usage over Time](#Plot-Usage-over-Time)\n",
    "        * [Fine Resolution](#Fine-Resolution)\n",
    "        * [Daily Resolution](#Daily-Resolution)\n",
    "    * [Plot SPICE Usage](#Plot-SPICE-Usage)\n",
    "    * [Plot Usage by User](#Plot-Usage-by-User)\n",
    "    * [Plot Usage by idl_analyst User](#Plot-Usage-by-idl_analyst-User)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup\n",
    "\n",
    "Import necessary libraries and set pandas display options."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import datetime\n",
    "import os\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.ticker import MaxNLocator\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "pd.set_option('display.max_rows', 16)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load and Process Data\n",
    "\n",
    "Load the text file of last month's IDL usage as a pandas dataframe and process it in preparation for producing plots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def get_last_month():\n",
    "    \"\"\"\n",
    "    Get the name of last month's logfile. The logfile will have name of the form 'mmyyyy.txt'.\n",
    "    \n",
    "    \"\"\"\n",
    "    today = datetime.date.today()\n",
    "    this_month = int(datetime.date.today().strftime('%m'))\n",
    "    this_year = int(datetime.date.today().strftime('%Y'))\n",
    "\n",
    "    if this_month == 1:\n",
    "        # Deal with a year change.\n",
    "        last_month = 12\n",
    "        year = this_year - 1\n",
    "    else:\n",
    "        last_month = this_month - 1\n",
    "        year = this_year\n",
    "\n",
    "    last_month = '{:02d}{}'.format(last_month, year)\n",
    "    return last_month"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# The environment variable `THIS_MONTH` is exported in the script run by the cron job\n",
    "# that produces the monthly logfile and this IDL usage analysis notebook.\n",
    "process_month = os.environ.get('THIS_MONTH')\n",
    "if process_month is None:\n",
    "    # Use as a fallback.\n",
    "    process_month = get_last_month()\n",
    "process_month"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Set up useful datetime formats.\n",
    "dt_snapshot = datetime.datetime.strptime(process_month, \"%m%Y\")\n",
    "dt_my = dt_snapshot.strftime(\"%m%Y\")\n",
    "dt = dt_snapshot.strftime(\"%b %Y\")\n",
    "dtp = dt_snapshot.strftime(\"%b_%Y\")\n",
    "dty = dt_snapshot.strftime(\"%Y\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Get last month's IDL usage log file.\n",
    "monthly_dir = '/project/avd/idl/idl_monthly_usage/monthly'\n",
    "filename = '{}.txt'.format(process_month)\n",
    "logfile = os.path.join(monthly_dir, filename)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Load IDL usage log file as pandas dataframe.\n",
    "col_names = ['time', 'license_type', 'user', 'host']\n",
    "monthly_log = pd.read_csv(logfile, sep=',',\n",
    "                          header=None, names=col_names,\n",
    "                          usecols=[0, 1, 2, 3], parse_dates=[0])\n",
    "monthly_log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fine Resolution Totals\n",
    "\n",
    "Get total usage over the month at the time resolution of the polling of the license server (approximately 15 minute intervals)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "fine_usage = monthly_log.groupby('time').size()\n",
    "fine_usage"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "daily_usages = monthly_log.reset_index(drop=True)\n",
    "daily_usages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Daily Totals\n",
    "\n",
    "Get daily total usage over the month."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Reduce time resolution to daily resolution.\n",
    "daily_usages['time'] = daily_usages['time'].apply(lambda t: t.strftime('%Y-%m-%d'))\n",
    "daily_usages['time'] = daily_usages['time'].apply(lambda t: datetime.datetime.strptime(t, '%Y-%m-%d'))\n",
    "daily_usages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "daily_usage = daily_usages.groupby('time').size()\n",
    "daily_usage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### SPICE Usage\n",
    "\n",
    "Get usage on SPICE in the month."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "spice_only = monthly_log[monthly_log['host'].str.contains(\"spice\")]\n",
    "user_spice = spice_only.groupby('user').size()\n",
    "user_spice"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Usage by User"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "user_idl = monthly_log.groupby('user').size()\n",
    "user_idl[user_idl > 1000]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Usage by `idl_analyst` User"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "idl_analyst_only = monthly_log[monthly_log[\"license_type\"].str.contains(\"analyst\")]\n",
    "user_imsl = idl_analyst_only.groupby(\"user\").size()\n",
    "user_imsl"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analysis and Plotting\n",
    "\n",
    "Let's plot the data contained in the IDL usage log for the past month.\n",
    "\n",
    "### Generic Plotting Function\n",
    "\n",
    "First off, let's define a plotting function that we can call later to make each plot that we need."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def plotter(df, kind, xlabel, ylabel, title, **sort_kw):\n",
    "    # Set up plot.\n",
    "    plt.figure(figsize=[20, 16])\n",
    "\n",
    "    plot_rolling_mean = sort_kw.pop('rolling_mean', None)\n",
    "    if len(sort_kw):\n",
    "        df = df.sort_values(**sort_kw)\n",
    "\n",
    "    # Plot dataframe.\n",
    "    if kind == 'line':\n",
    "        ax = df.plot(y=1, kind=kind)\n",
    "        if plot_rolling_mean:\n",
    "            # There are 96 15min periods in a day.\n",
    "            periods = 24 * (60 / 15)\n",
    "            rolling_mean = pd.rolling_mean(df,\n",
    "                                           window=periods,\n",
    "                                           center=True)\n",
    "            rolling_mean.plot(y=1, kind=kind, color='r', lw=2)\n",
    "    else:\n",
    "        ax = df.plot(kind=kind)\n",
    "\n",
    "    # Add information to plot.\n",
    "    ax.set_ylim(ymin=0)\n",
    "    ax.set_xlabel(xlabel, fontsize=20)\n",
    "    ax.set_ylabel(ylabel, fontsize=20)\n",
    "    ax.set_title(title, fontsize=24)\n",
    "    \n",
    "    # Show!\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot Usage over Time\n",
    "\n",
    "Plot IDL usage on each day for the past month.\n",
    "\n",
    "#### Fine Resolution\n",
    "\n",
    "First we plot IDL usage at the resolution provided by polling the license server (at approximately 15 minute intervals)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "xlabel = \"Date ({})\".format(dt)\n",
    "ylabel = 'License Usage'\n",
    "title = 'Total IDL License Use on Scientific Desktop for {}'\n",
    "\n",
    "plotter(fine_usage, 'line', xlabel, ylabel, title.format(dt), rolling_mean=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Daily Resolution\n",
    "\n",
    "Now plot IDL usage at the resolution of usage per day."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "xlabel = \"Date ({})\".format(dt)\n",
    "ylabel = 'License Usage'\n",
    "title = 'Total Daily IDL License Use on Scientific Desktop for {}'\n",
    "\n",
    "plotter(daily_usage, 'line', xlabel, ylabel, title.format(dt))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot Usage by top Users\n",
    "\n",
    "Plot amount of IDL usage by the top users (not on SPICE). For the sake of clarity of the following plot, this plot is truncated by users who have invoked IDL more than 1000 times within the month."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "cutoff = 1000\n",
    "\n",
    "users_gt_1000 = user_idl[user_idl > cutoff].sort_values(ascending=False)\n",
    "\n",
    "with pd.option_context(\"display.max_rows\", None):\n",
    "    print users_gt_1000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "title = \"IDL Call Count for {} (>{} invocations)\".format(dt, cutoff)\n",
    "plotter(users_gt_1000, 'bar', 'User', 'License Usage', title, axis=0, ascending=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot SPICE Usage\n",
    "\n",
    "Plot IDL usage on SPICE by user."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "cutoff = 100\n",
    "title = \"IDL Call Count on SPICE for {} (>{} invocations)\".format(dt, cutoff)\n",
    "plotter(user_spice[user_spice > cutoff], 'bar', 'User', 'License Usage', title, axis=0, ascending=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot Usage by `idl_analyst` User\n",
    "\n",
    "Plot amount of IDL usage by the `idl_analyst` (IMSL) user."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "title = \"IMSL Call Count for {}\".format(dt)\n",
    "\n",
    "plotter(user_imsl, 'bar', 'User', 'License Usage', title, axis=0, ascending=False)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
