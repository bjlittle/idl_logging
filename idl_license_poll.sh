#!/usr/bin/env bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

PYTHON=/opt/scitools/environments/default_legacy/current/bin/python

# Get the directory of this script.
export SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

# Capture output to a standard logfile location.
LOG_DIR=${SCRATCH}/logs
mkdir -p ${LOG_DIR}
LOGFILE=${LOG_DIR}/idl_license_poll.$(date +%Y-%m-%d-%H%M%S).log

unbuffer ${PYTHON} ${SCRIPT_DIR}/license_poll_python.py >>${LOGFILE} 2>&1
