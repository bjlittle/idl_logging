import license_poll_python as LPP
import os

PARSE_DIR = "/net/home/h05/bclarke/parse_testing"


def test_license_poll_file_exists():
    log_instance = LPP.Main()
    log_instance.logger()
    assert log_instance.file_name in os.listdir(PARSE_DIR)


def test_license_poll_file_not_empty():
    log_instance = LPP.Main()
    log_instance.logger()
    assert open(os.path.join(PARSE_DIR, log_instance.file_name), 'r') != ""

