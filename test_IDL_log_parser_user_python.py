import IDL_log_parser_user_python as parser
import os
import datetime

PARSE_DIR = "/net/home/h05/bclarke/parse_testing"
TEST_DIR = "/net/home/h05/bclarke/testing_parse_script"


# LINE PROCESSOR:
def test_line_processor_form():
    line_test = parser.Main()
    assert line_test.line_processor(
        "user_code line_stuff more_line_stuff"
        ) == "user_code, line_stuff, more_line_stuff"


# NAME FINDER:
def test_name_finder_name_exists():
    name_test = parser.Main()
    assert name_test.name_finder("apaj") == "alistair.manning"


def test_name_finder_no_name_exists():
    name_test = parser.Main()
    assert name_test.name_finder("flash") == "flash"


def test_name_finder_name_in_dict():
    name_test = parser.Main()
    name_test.names_dict['apaj'] = 'alistair.manning'
    assert name_test.name_finder("apaj") == "alistair.manning"


def test_name_finder_add_to_dict():
    name_test = parser.Main()
    name_test.name_finder("apaj")
    assert name_test.names_dict['apaj'] == 'alistair.manning'


# SUMMARY MAKER THING

# pass a known file handle - this should then loop through that and write out
# to the monthly file
# could redefine the monthly file in some special directory
def test_sub_summary_maker_file_exists():
    summary_maker = parser.Main()
    summary_maker.monthly_summary = open(
        os.path.join(TEST_DIR, summary_maker.fname+'.txt'), 'w'
        )
    summary_maker._summary_maker(
        os.path.join(PARSE_DIR, '20170804112134')
        )
    assert summary_maker.fname+'.txt' in os.listdir(TEST_DIR)
    os.remove(os.path.join(TEST_DIR, summary_maker.fname+'.txt'))


def test_sub_summary_maker_file_not_empty():
    summary_maker = parser.Main()
    summary_maker.monthly_summary = open(
        os.path.join(TEST_DIR, summary_maker.fname+'.txt'), 'w'
        )
    summary_maker._summary_maker(
        os.path.join(PARSE_DIR, '20170804112134')
        )
    assert open(os.path.join(TEST_DIR, summary_maker.fname+'.txt')) != ""
    os.remove(os.path.join(TEST_DIR, summary_maker.fname+'.txt'))

