import pandas_processing as PP
import pandas as pd
import numpy as np
import os
import datetime

TEST_DIR = '/net/home/h05/bclarke/testing_pandas_script/'


# MONTHLY DATA FINDER
def test_monthly_data_finder_edge_cases():
    months_test = PP.Main()
    assert months_test.monthly_data_finder('032000') == [
        '032000_df.txt', '022000_df.txt', '012000_df.txt',
        '121999_df.txt', '111999_df.txt', '101999_df.txt',
        '091999_df.txt', '081999_df.txt', '071999_df.txt',
        '061999_df.txt', '051999_df.txt', '041999_df.txt'
        ]

def test_monthly_data_finder_easy_case():
    months_test = PP.Main()
    assert months_test.monthly_data_finder('122012') == [
        '122012_df.txt', '112012_df.txt', '102012_df.txt',
        '092012_df.txt', '082012_df.txt', '072012_df.txt',
        '062012_df.txt', '052012_df.txt', '042012_df.txt',
        '032012_df.txt', '022012_df.txt', '012012_df.txt'
        ]


# PLOTTING
def test_plotter_monthly():
    plot_test = PP.Main()
    # Change directory to save to
    plot_test.monthly_dir = TEST_DIR
    # Manually create DF
    plot_test.indexed_monthly = pd.DataFrame(
        np.random.randint(low=0, high=10, size=(5, 2))
        )
    plot_test.plotter_monthly()
    fname = 'Daily_usage_{}.png'.format(plot_test.dtp)
    assert fname in os.listdir(TEST_DIR)
    os.remove(os.path.join(TEST_DIR, fname))


def test_plotter_idl_user():
    plot_test = PP.Main()
    # Change directory to save to
    plot_test.HOME_DIR = TEST_DIR
    # Manually create DF
    plot_test.user_idl = pd.Series(np.random.randint(low=0, high=10, size=(5)))
    plot_test.plotter_idl_user()
    fname = 'IDL_Users{}.png'.format(plot_test.dtp)
    assert fname in os.listdir(TEST_DIR)
    os.remove(os.path.join(TEST_DIR, fname))


def test_plotter_imsl_user():
    plot_test = PP.Main()
    # change directory to save to
    plot_test.HOME_DIR = TEST_DIR
    # Manually create DF
    plot_test.user_imsl = pd.Series(
        np.random.randint(low=0, high=10, size=(5))
        )
    plot_test.plotter_imsl_user()
    fname = 'IMSL_Users{}.png'.format(plot_test.dtp)
    assert fname in os.listdir(TEST_DIR)
    os.remove(os.path.join(TEST_DIR, fname))


def test_plotter_spice_user():
    plot_test = PP.Main()
    # change directory to save to
    plot_test.HOME_DIR = TEST_DIR
    # Manually create DF
    plot_test.user_spice = pd.Series(
        np.random.randint(low=0, high=10, size=(5))
        )
    plot_test.plotter_spice_user()
    fname = 'IDL_Users_SPICEP{}.png'.format(plot_test.dtp)
    assert fname in os.listdir(TEST_DIR)
    os.remove(os.path.join(TEST_DIR, fname))


def test_plotter_yearly():
    plot_test = PP.Main()
    # Change directory to save to
    plot_test.HOME_DIR = TEST_DIR
    # Manually create DF
    plot_test.indexed_yearly = pd.concat(
        [pd.DataFrame(np.random.randint(low=0, high=10, size=(5, 2)))]*12
        )
    plot_test.plotter_yearly()
    fname = 'Daily_usage_{}.png'.format(plot_test.dty)
    assert fname in os.listdir(TEST_DIR)
    os.remove(os.path.join(TEST_DIR, fname))

