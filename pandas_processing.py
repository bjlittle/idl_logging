"""
Script to process and plot IDL usage data.

Accesses monthly summary files of IDL license information, within monthly
directory. Use pandas methods to process the monthly data files for plotting.

"""

import datetime
import os

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import pandas as pd


class Main(object):
    """
    Processes and visualises monthly summary data of IDL usage.

    Args: None

    """

    def __init__(self, date_ref=None):
        # Optionally override running the processing based on "now".
        self.date_ref = date_ref

        # Set important directories and date times.
        self.plot_dir = '/project/avd/idl/idl_monthly_usage/plots/'
        self.monthly_dir = '/project/avd/idl/idl_monthly_usage/monthly/'

        # Set up useful date-time formats.
        self.dt_snapshot = datetime.datetime.now()
        if self.date_ref is not None:
            try:
                self.dt_snapshot = datetime.datetime.strptime(self.date_ref,
                                                              "%m%Y")
            except ValueError:
                pass

        self.dt_my = self.dt_snapshot.strftime("%m%Y")
        self.dt = self.dt_snapshot.strftime("%b %Y")
        self.dtp = self.dt_snapshot.strftime("%b_%Y")
        self.dty = self.dt_snapshot.strftime("%Y")
        # Load in file for current month.
        self.monthly_log = pd.read_csv(
            os.path.join(self.monthly_dir, "{}{}".format(self.dt_my, '.txt')),
            sep=',', header=None, usecols=[0, 1, 2, 3]
            )

    def daily_totals(self):
        """
        Uses pandas 'groupby' method to process input monthly summary data to
        show total daily idl license usage per day, over all users.
        Converts data into a format that may be visualised.

        """
        # Extract the total number of users per day.
        daily_usage = self.monthly_log.groupby(0).size()
        daily_usage.to_csv(os.path.join(self.monthly_dir,
                                        "{}{}".format(self.dt_my, '_df.txt')))

    def monthly_info(self):
        """
        Uses pandas 'groupby' method to process input monthly summary data by
        user of different license types (idl, spice, and idl_analyst).

        Converts data into a format that may be visualised.

        """
        # Extract monthly usage by user for idl, idl_analyst and SPICE.
        self.user_idl = self.monthly_log.groupby(2).size()

        self.spice_only = self.monthly_log[
            self.monthly_log[3].str.contains("spice")
            ]
        self.user_spice = self.spice_only.groupby(2).size()

        idl_analyst_only = self.monthly_log[
            self.monthly_log[1].str.contains("analyst")
            ]
        self.user_imsl = idl_analyst_only.groupby(2).size()

    def monthly_data_finder(self, mmyyyy):
        """
        Return a list of the 12 most recent monthly summary text
        file names, given an input month.

        Args:

            * mmyyyy = string of date in terms of month and year

        Returns:

            A list of 12 monthly file names preceding input month

        """
        # Assign first element as current month.
        month_list = [mmyyyy+'_df.txt']
        # Variable to deal with the previous year.
        last_yr_month = 120000
        # Deal with months 01-09.
        if self.dt_my[0] == 0:
            mmyyyy = int(mmyyyy[1:])
        # Loop over 11 elements of list.
        for i in range(11):
            # Algorithm to return the previous 11 months in order:
            # Firstly calculate a quantity for each element, to test against.
            past_month = str(int(mmyyyy) - (10000 * (i+1)))
            last_yr = int(mmyyyy[-4:]) - 1
            # For months in the same year as the current month, add to list.
            if int(past_month) > 10000:
                month_list.append("{}{}".format(past_month, '_df.txt'))
            # If in a previous year, construct file name and track using var.
            else:
                past_month = str(last_yr_month + last_yr)
                last_yr_month -= 10000
                month_list.append("{}{}".format(past_month, '_df.txt'))
            # Re-add '0' to the beginning of the necessary months.
            if len(month_list[i+1]) == 12:
                month_list[i+1] = '0' + month_list[i+1]
        return month_list

    def load_yearly_info(self):
        """
        Load monthly summary files for the past year and combine
        into an up-to-date yearly file.

        Args: None

        Returns:

            A list of 12 monthly file names preceding input month
            A combined dataframe containing information from 12 months
            of logs

        """
        # Getting the data:
        # Acquire the 12 previous monthly filenames.
        monthly_data = self.monthly_data_finder(
            self.dt_snapshot.strftime("%m%Y"))

        # Load these files in using pandas and add to a list.
        frames = []
        for month in monthly_data:
            if month in os.listdir(self.monthly_dir):
                month_file = pd.read_csv(
                    "{}{}".format(self.monthly_dir, month),
                    sep=',', header=None
                    )
                frames.append(month_file)

        # Concatenate list to create single object for entire year.
        yearly = pd.concat(frames)
        return monthly_data, yearly

    def indexer(self):
        """
        Add a date-time index to the daily usage dataframes over the most
        recent month and year logs.

        """
        monthly_data, yearly = self.load_yearly_info()
        # Set the index of the yearly and monthly daily usage files to be one
        # per day.
        daily_usage = pd.read_csv(
            "{}{}".format(self.monthly_dir, monthly_data[0]),
            sep=',', header=None
            )

        self.indexed_monthly = daily_usage.set_index(
            pd.date_range(self.dt_my[:2] + '/1/' +
                          self.dt_my[2:6], periods=len(daily_usage))
            )

        self.indexed_yearly = yearly.set_index(
            pd.date_range(monthly_data[-1][:2] + '/1/' +
                          monthly_data[-1][2:6], periods=len(yearly))
            )

    def _plotter(self, what, kind, xlabel, ylabel, title, filename, **sort_kw):
        """
        A generic plotting method. Called by specific public methods to produce
        specific plots.

        """
        plt.figure(figsize=[20, 16])

        df = getattr(self, what)
        if len(sort_kw):
            df.sort(**sort_kw)

        if kind == 'line':
            ax = df.plot(y=1, kind=kind)
        else:
            ax = df.plot(kind=kind)
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.set_xlabel(xlabel, fontsize=24)
        ax.set_ylabel(ylabel, fontsize=24)
        ax.set_title(title, fontsize=24)

        plt.tight_layout()
        plt.savefig(os.path.join(self.plot_dir, filename))
        plt.close()

    def plotter_monthly(self):
        """
        Plot IDL daily usage for 1 month, and save as image.

        """
        xlabel = "Date ({})".format(self.dt)
        ylabel = 'License Usage'
        title = 'Total Daily IDL License Use on Scientific Desktop for {}'
        plot_filename = 'Daily_usage_{}.png'.format(self.dtp)

        self._plotter('indexed_monthly', 'line',
                      xlabel, ylabel, title.format(self.dt), plot_filename)

    def plotter_idl_user(self):
        """
        Plot IDL usage by user for 1 month, and save as image.

        """
        title = "IDL Call Count for {}".format(self.dt)
        plot_filename = 'IDL_users_{}'.format(self.dtp)

        self._plotter('user_idl', 'bar',
                      'User', 'License Usage', title, plot_filename,
                      ascending=0)

    def plotter_imsl_user(self):
        """
        Plot IMSL usage by user for 1 month, and save as image.

        """
        title = "IMSL Call Count for {}".format(self.dt)
        plot_filename = "IMSL_users_{}.png".format(self.dtp)

        self._plotter('user_imsl', 'bar',
                      'User', 'License Usage', title, plot_filename,
                      ascending=0)

    def plotter_spice_user(self):
        """
        Plot IDL (SPICE) usage by user for 1 month, and save as image.

        """
        title = "IDL Call Count for {} (SPICE)".format(self.dt)
        plot_filename = "IDL_Users_SPICE_{}.png".format(self.dtp)

        self._plotter('user_spice', 'bar',
                      'User', 'License Usage', title, plot_filename,
                      ascending=0)

    def plotter_yearly(self):
        """
        Plot IDL daily usage for 1 year, and save as image.

        """
        xlabel = "Date ({})".format(self.dt)
        title = 'Total Daily IDL License Use on Scientific Desktop for {}'
        plot_filename = "Daily_usage_{}.png".format(self.dty)

        self._plotter('indexed_yearly', 'line',
                      xlabel, 'License Usage', title.format(self.dty),
                      plot_filename)


if __name__ == "__main__":
    import sys
    # Check for a specified date-time string to process at.
    try:
        dt_str = str(sys.argv[1])
    except IndexError:
        dt_str = None

    processing_instance = Main(dt_str)
    processing_instance.daily_totals()
    processing_instance.monthly_info()
    processing_instance.indexer()
    processing_instance.plotter_monthly()
    processing_instance.plotter_idl_user()
    processing_instance.plotter_imsl_user()
    processing_instance.plotter_spice_user()
    processing_instance.plotter_yearly()

