"""
Polls IDL license server, and returns a file containing usage information into
the logs directory.

"""

import subprocess
import datetime
import os


class Main(object):
    """
    Class that executes a poll of an IDL license server and writes the output
    to a new file.

    Args: None

    """
    def __init__(self):
        # Location of directory to save logs files to.
        self.log_dir = "/project/avd/idl/idl_monthly_usage/logs/"
        # Create name for file based on current time and date.
        self.file_name = datetime.datetime.now().strftime("%Y%m%d%H%M%S")

    def logger(self):
        # Create a new file in the save directory using open, and
        # poll the license server and write the output to the file.
        lmstat = "/opt/exelis/idl_default/bin/lmstat"
        script_dir = os.environ.get('SCRIPT_DIR')
        license_dat = os.path.join(script_dir, "exxvmlic01-license.dat")
        with open(os.path.join(self.log_dir, self.file_name), 'w') as log_file:
            log_file.write(subprocess.check_output([lmstat,
                                                    "-S", "-c",
                                                    license_dat]))


if __name__ == "__main__":
    log_instance = Main()
    log_instance.logger()

