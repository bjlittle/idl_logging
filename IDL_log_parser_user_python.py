"""
Script to parse IDL logs into a monthly summary file.

Accesses all IDL license logs within log directory, for the current month.
Loops through every line within file and extracts and saves the important
information (including date, license type, user codename, and user real name)
into a single output file. The output file is in a format designed for use by
the `pandas_processing` script which is used to process and plot the data.

"""

import os
import subprocess
import datetime
import re


class Main(object):
    """
    Class that parses a set of IDL log files into a single monthly summary
    file.

    Args: None

    """
    # Empty dictionary for use in name_finder.
    names_dict = {}

    def __init__(self, date_ref=None):
        self.date_ref = date_ref

        # Set up `self.date_ref` as a datetime object, if appropriate, and
        # create a new dated text file to write monthly info to.
        if self.date_ref is not None:
            self.dt_ref = datetime.datetime.strptime(self.date_ref, '%m%Y')
            self.fname = self.dt_ref.strftime("%m%Y")
        else:
            self.dt_ref = None
            self.fname = datetime.datetime.now().strftime("%m%Y")

        # Location of important directories: log files and monthly summaries.
        self.log_dir = '/project/avd/idl/idl_monthly_usage/logs'
        self.monthly_dir = os.environ.get('MONTHLY_DIR')

    def line_processor(self, string):
        """
        Process a line from the log file and return
        a format that is useful for data analysis.

        Args:

            * string: a line from an IDL log file

        Returns:

            A formatted version of Arg string (containing commas).

        """
        # Find position for first comma.
        pos = len(string.split()[0])
        # Replace excess white-space.
        string = re.sub(' +', ' ', string)
        string = string.lstrip()
        # Find position for second comma.
        pos2 = pos + 1 + len(string.split()[1])
        # Add commas into the appropriate places.
        string = "{},{},{}".format(string[:pos],
                                   string[pos:pos2],
                                   string[pos2:])
        return string

    def name_finder(self, string):
        """
        Process a line from the log file, returning the
        same line with the user's code-name replaced by an actual name (where
        applicable).

        Args:

            * string = a line from an IDL log file

        Returns: A formatted version of Arg string (containing users name)

        """
        # Slice out user code-name.
        user_code = string.split()[0]
        # Avoid reprocessing the same user.
        if user_code not in self.names_dict:
            finger_string = subprocess.check_output(
                ['finger', user_code]
                )
            # Assign real names where possible.
            if finger_string != '':
                name = finger_string.split()[3]
                self.names_dict[user_code] = name
            else:
                # In the absence of a real name, use the codename.
                name = user_code
                self.names_dict[user_code] = name
        # For user already fingered, use real name rather than codename.
        else:
            name = self.names_dict[user_code]
        string = string.replace(user_code, name)
        return string

    def _summary_maker(self, filename):
        """
        Open up a log file, loop through it's contents, and write
        out the important information in a useful format, in a summary file.

        Args:

            * handle = string through which to access target file

        Returns: None

        """
        dts = []
        lictypes = []
        lines = []
        dt = datetime.datetime.strptime(filename, '%Y%m%d%H%M%S')
        summary_filename = "{}.txt".format(self.fname)
        summary_file = os.path.join(self.monthly_dir, summary_filename)
        with open(os.path.join(self.log_dir, filename), 'r') as handle:
            # Set variables for License type and skip count.
            lictype = "NaN"
            skip_count = 12
            # Open current file and loop over all lines within.
            for line in handle:
                # Skip unwanted lines - initial and empty.
                if skip_count > 0:
                    skip_count -= 1
                    continue
                elif line[0:8] == "":
                    continue
                # Capture license type by splitting line.
                elif line[0:8] == "Users of":
                    lictype = line.split(" ")[2]
                    skip_count = 4
                else:
                    if len(line) > 1:
                        # Find real name of users and replace code.
                        line = self.name_finder(line)
                        # Split line up into important sections:
                        # Isolate user name and server name.
                        line = self.line_processor(line)
                        dts.append(dt)
                        lictypes.append(lictype)
                        lines.append(line)

        with open(summary_file, 'a') as monthly_summary:
            # Write info to file.
            for dt, lictype, line in zip(dts, lictypes, lines):
                monthly_summary.write("{},{},{}".format(dt, lictype, line))

    def summary_maker(self):
        """
        Create a list of log files from the current month of study,
        and create a handle for each to pass to `_summary_maker`.

        """
        if self.date_ref is None:
            dt_check = datetime.datetime.now().strftime("%Y%m")
        else:
            dt_check = self.dt_ref.strftime("%Y%m")
        file_list = [f for f
                     in os.listdir(self.log_dir)
                     if f.startswith(dt_check)
                     ]
        # Loop over all files in log.
        for filename in file_list:
            # Run summary maker.
            self._summary_maker(filename)


if __name__ == "__main__":
    import sys
    try:
        date_ref = str(sys.argv[1])
    except IndexError:
        date_ref = None

    monthly_summary_instance = Main(date_ref)
    monthly_summary_instance.summary_maker()

