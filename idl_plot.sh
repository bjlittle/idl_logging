#!/usr/bin/env bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

# Set up executables and input/output files and folder locations.
PYTHON="/opt/scitools/environments/default_legacy/current/bin/python"
JUPYTER="/opt/scitools/environments/default_legacy/current/bin/jupyter"
MONTHLY_NOTEBOOK="${SCRIPT_DIR}/idl_monthly_usage_analysis.ipynb"
USAGE_NOTEBOOK="${SCRIPT_DIR}/idl_usage_over_time.ipynb"
OUTPUT_DIR="/project/avd/idl/idl_monthly_usage/notebook"
export MONTHLY_DIR="/project/avd/idl/idl_monthly_usage/monthly"
export THIS_MONTH=$(date '+%m%Y')

# Specify executed notebooks names.
OUTPUT_NAME_MONTHLY="idl_monthly_usage_analysis_${THIS_MONTH}.ipynb"
OUTPUT_NAME_USAGE="idl_usage_over_time.ipynb"  # Overwrite this each month to get rolling stats.

# Capture output to a standard logfile location.
LOG_DIR=${SCRATCH}/logs
mkdir -p ${LOG_DIR}
LOGFILE=${LOG_DIR}/idl_plot.$(date +%Y-%m-%d-%H%M%S).log

# Produce IDL monthly usage file.
unbuffer ${PYTHON} ${SCRIPT_DIR}/IDL_log_parser_user_python.py ${THIS_MONTH} >>${LOGFILE} 2>&1

# Produce IDL daily invocations file.
unbuffer ${PYTHON} ${SCRIPT_DIR}/IDL_log_parser_daily_usage.py >>${LOGFILE} 2>&1

# Generate monthly usage notebook.
unbuffer ${JUPYTER} nbconvert --to notebook \
                              --allow-errors \
                              --ExecutePreprocessor.timeout=300 \
                              --output-dir=${OUTPUT_DIR} \
                              --output=${OUTPUT_NAME_MONTHLY} \
                              --execute \
                              ${MONTHLY_NOTEBOOK} >> ${LOGFILE} 2>&1

# Generate notebook showing usage over time.
unbuffer ${JUPYTER} nbconvert --to notebook \
                              --allow-errors \
                              --ExecutePreprocessor.timeout=300 \
                              --output-dir=${OUTPUT_DIR} \
                              --output=${OUTPUT_NAME_USAGE} \
                              --execute \
                              ${USAGE_NOTEBOOK} >> ${LOGFILE} 2>&1
