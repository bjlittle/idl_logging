"""
Parse the monthly log file produced in `IDL_log_parser_user_python.py` to find
total number of IDL invocations per day in the month. Daily usages are saved
to the same file location as the imput log file.

"""

import os

import pandas as pd


def load_and_resample(logfile):
    """
    Load the processed monthly `logfile` as a pandas dataframe.

    Then perform two bits of processing to the dataframe:
        1. Count total IDL usage per day by counting number of invocations
        2. Count unique IDL users per day.

    """
    counts = []

    # Load dataframe.
    col_names = ['time', 'license_type', 'user', 'host']
    df = pd.read_csv(logfile, sep=',',
                     header=None, names=col_names,
                     index_col=0, usecols=[0, 1, 2, 3], parse_dates=[0])

    # 1. setup for counting IDL usage per day.
    daily_df = df.resample('D').count()
    days = daily_df.index

    # 2. setup for counting unique IDL users per day.
    df = df.reset_index()
    df['time'] = df['time'].apply(lambda ts: ts.round('D'))

    # Now bring everything together per day into the counts dict.
    for day in days:
        # Complete finding (1.) for this day.
        usage_per_day = daily_df.loc[day]['license_type']
        # Complete finding (2.) for this day.
        users_per_day = len(df[df['time'] == day]['user'].unique())
        # Assemble each day's entry in the counts dict.
        key = pd.to_datetime(day).strftime('%Y-%m-%d')
        counts.append([key, usage_per_day, users_per_day])
    return counts


def saver(monthly_dir, month, daily_counts):
    """
    Write the contents of interest into another text file in the monthly
    directory.

    """
    logfile = '{}_daily.txt'.format(month)
    filename = os.path.join(monthly_dir, logfile)
    with open(filename, 'a') as olfh:
        for (k, usage, users) in daily_counts:
            olfh.write('{},{},{}\n'.format(k, usage, users))


def main():
    monthly_dir = os.environ.get('MONTHLY_DIR')
    month = os.environ.get('THIS_MONTH')
    processed_logfile_name = '{}.txt'.format(month)
    logfile = os.path.join(monthly_dir, processed_logfile_name)

    daily_counts = load_and_resample(logfile)
    saver(monthly_dir, month, daily_counts)


if __name__ == '__main__':
    main()

