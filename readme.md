# IDL License Usage logging

## What is it?

Logs usages of IDL within the Met Office estate by polling the IDL license server.
Usage statistics are collected at 15 minute intervals and converted into monthly usage
statistics, which are released as a series of charts.


## How does it work?

IDL logging is controlled by two cron jobs that run on `els056` and are managed by cronanny.
The cron jobs can be found here: https://exxgitrepo:8443/projects/AVD/repos/crons/browse.
The crons perform the following functions:

### Cron 1

* Run by the `avd` user (but see note at bottom of readme).
* Runs every 15 minutes.
* Polls the IDL License server and produces a log file
of instantaneous IDL license usages.
* The cron job executes
`idl_license_poll.sh`, which in turn runs the Python script `license_poll_python.py`
that performs the IDL icense server polling with `exxvmlic01-license.dat`.


### Cron 2

* Run by the `avd` user (but see note at bottom of readme).
* Runs once a month.
* Processes all log files for the month, extracting
key information from each log file and saving it to a single monthly IDL usage file.
* Produces IDL usage plots for the month.
* The cron job executes `idl_plot.sh`, which runs the Python script
`IDL_log_parser_user_python.py` to produce the single monthly IDL usage file,
and runs `pandas_processing.py` to produce IDL usage plots for the month.


## Where are all the files?

The files are all stored on the `/project` disk in the following locations:

* IDL usage log files: `/project/avd/idl/idl_monthly_usage/logs`.
* Processed monthly IDL usage files: `/project/avd/idl/idl_monthly_usage/monthly`.
* Usage plots: `/project/avd/idl/idl_monthly_usage/plots`.

All these folder locations are owned by the `idl` user.


## Everything has gone wrong, what do I do?

* Check the AVD Support inbox for failed job notifications.
* Check the log files at `/scratch/avd/logs/idl_plot.<date>.log`.
* Check if there are IDL license usage files being saved to
`/project/avd/idl/idl_monthly_usage/logs`.
* Check if there are monthly usage files being saved to
`/project/avd/idl/idl_monthly_usage/monthly`.
* Check if there are IDL usage plots being saved to
`/project/avd/idl/idl_monthly_usage/plots`.
* Neither the `avd` user nor the `idl` user can run the `module` command.
To work around this we hard-coded the location of the default-current python
executable. Check that this is still available and update as appropriate!

Note: it is expected that the crons will be migrated to being run by the `idl`
user rather than the `avd` user, which was correct at time of writing.